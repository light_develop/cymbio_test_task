<?php

class Cymbio_Test_Model_Observer
{
    /**
     * @var string
     */
    protected $extraCartButtonTemplate = 'cymbio/catalog/product/view/addtocart.phtml';

    /**
     * @var string
     */
    protected $productAddToCartBlockName = 'product.info.addtocart';

    /**
     * Prepare Extra Add To Cart Button
     * @param \Mage_Catalog_Model_Product $product
     * @return string
     */
    private function getExtraAddToCartButtonHtml(Mage_Catalog_Model_Product $product)
    {
        $block = Mage::app()->getLayout()
            ->createBlock('core/template')
            ->setProduct($product)
            ->setTemplate($this->extraCartButtonTemplate);

        return $block instanceof Mage_Core_Block_Template ? $block->toHtml() : "";
    }

    /**
     * Add Extra Button to any page, where add to cart button is rendered and where current_product exists
     * @param \Varien_Event_Observer $observer
     * @return void
     */
    public function addExtraButton(Varien_Event_Observer $observer)
    {
        /** @var Mage_Core_Block_Abstract $block */
        $block = $observer->getEvent()->getBlock();
        $transport = $observer->getEvent()->getData("transport");

        if ($block->getNameInLayout() == $this->productAddToCartBlockName) {
            $product = $block->getProduct();

            if ($product instanceof Mage_Catalog_Model_Product && $product->isSalable()) {
                $transport->setHtml(
                    $transport->getHtml() . $this->getExtraAddToCartButtonHtml($product)
                );
            }
        }
    }

    public function addProductToCart(Varien_Event_Observer $observer)
    {
        /** @var Mage_Core_Controller_Request_Http $request */
        $request = $observer->getEvent()->getRequest();
        /** @var Mage_Catalog_Model_Product $product */
        $product = $observer->getEvent()->getProduct();

        try {
            if ((int)$request->getParam('use_cymbio')) {
                $this->getApiHandler()->call(array('product' => $product));
            }
        } catch(Exception $e) {
            Mage::logException($e);
        }
    }

    /**
     * @return \Cymbio_Test_Model_Spi_ApiInterface
     */
    public function getApiHandler()
    {
        return Mage::getSingleton('cymbio_test/api_event');
    }
}