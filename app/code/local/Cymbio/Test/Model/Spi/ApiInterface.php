<?php

interface Cymbio_Test_Model_Spi_ApiInterface
{
    public function call(array $params = array());
}