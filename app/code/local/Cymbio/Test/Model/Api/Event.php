<?php

class Cymbio_Test_Model_Api_Event implements Cymbio_Test_Model_Spi_ApiInterface
{
    private function validateParams($params)
    {
        if (!isset($params['product'])) {
            throw new Exception("Product must be set");
        }
    }
    /**
     * @param \Mage_Catalog_Model_Product $product
     * @return Zend_Http_Response
     */
    public function call(array $params = array())
    {
        $this->validateParams($params);
        $configProcessor = $this->getConfigProcessor();
        $store = Mage::app()->getStore();
        /** @var Mage_Catalog_Model_Product $product */
        $product = $params['product'];

        //Registering entities
        $configProcessor->registerEntity('store', $store);
        $configProcessor->registerEntity('product', $product);
        $configProcessor->setConfigPath("default/add_to_cart_cymbio_request");
        /** @var array $params */
        $params = $configProcessor->process();

        //Set config path
        $configProcessor->setConfigPath('default/add_to_cart_cymbio_request/params');
        $params['params'] = $configProcessor->process();

        $builder = $this->getRequestBuilder();
        /** @var Zend_Http_Client $client */
        $client = $builder->build($params);

        /** @var Zend_Http_Response $response */
        $response = $client->request();
        $this->registerCymbioResponse($response, $params);

        Mage::dispatchEvent('cymbio_request_sended',
            array('product' => $product, 'response' => $response, 'client' => $client,
                'cymbio_event' => Cymbio_Test_Model_Event::DEFAULT_EVENT));

        return $response;
    }

    /**
     * @return false|\Cymbio_Test_Model_Cymbio_Events_Builder
     */
    private function getRequestBuilder()
    {
        return Mage::getModel("cymbio_test/cymbio_events_builder");
    }

    /**
     * @return false|\Cymbio_Test_Model_Cymbio_Events_ConfigProcessor
     */
    private function getConfigProcessor()
    {
        return Mage::getModel("cymbio_test/cymbio_events_configProcessor");
    }

    /**
     * @param \Zend_Http_Response $response
     * @param array $requestParams
     * @return void
     */
    private function registerCymbioResponse(Zend_Http_Response $response, array $requestParams)
    {
        Mage::log([
            "response" => $response->getMessage(),
            "client" => $requestParams
        ],
            null,
            'cymbio_request.log'
        );
    }
}

